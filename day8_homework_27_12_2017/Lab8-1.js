// const temp = require('./employee.js');
// const Employee = temp.Employee;

const {Employee} = require('./employee.js');
const {CEO} = require('./ceo.js');

let dang = new Employee('Dang', 'Red', 10000);
let kan = new Employee('Kan', 'Green', 20000);
let ceo = new CEO('Somchai', 'Sudlor', 30000);
console.log(ceo.getSalary());
// ceo.order(kan);

ceo.hello();
dang.hello();
kan.hello();