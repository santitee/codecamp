const fs = require('fs');
const {Employee} = require('./employee.js');
const {Programmer} = require('./programmer.js');

class CEO extends Employee {
    constructor(firstname, lastname, salary, dressCode) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        this.employeesRaw = [];
        this.employees = [];
    }
    getSalary() {  // simulate public method
        return super.getSalary() * 2;
    }
    work(employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)
    }
    _fire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " has been fired! Dress with :" + this.dressCode);
    }
    _hire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " has been hired back! Dress with :" + this.dressCode);
    }
    _seminar() { // simulate private method
        this.dressCode = 'suit';
        console.log("He is going to seminar Dress with :" + this.dressCode);
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection. Dress with :" + this.dressCode);
    }
    gossip(employee) {
        console.log("Hey " + employee.firstname, "Today is very cold!");
    }
    async readFile() {
        let self = this;
        try {
            await new Promise(function (resolve, reject) {
                fs.readFile('homework1.json', 'utf8', function (err, data) {
                    if(err)
                    reject(err)
                    else { 
                        let employees = [];
                        self.employeesRaw = JSON.parse(data)
                        for(let i = 0; i < self.employeesRaw.length; i++){
                            employees[i] = new Programmer(
                                self.employeesRaw[i].firstname,
                                self.employeesRaw[i].lastname,
                                self.employeesRaw[i].salary,
                                self.employeesRaw[i].id)
                        }
                        self.employees = employees;
                        resolve(data)
                    }
                });
            })
        } catch (err) {
            console.error(err);
        }
    }
}
exports.CEO = CEO;