const {Employee} = require('./employee.js');

class Programmer extends Employee {
    constructor( firstname, lastname, salary, id, type = "Programmer") {
        super(firstname, lastname, salary);
        this.id = id
        this.type = type;
    }
    work(employee) {  // simulate public method
        this._createwebsite(employee);
        this._fixpc(employee);
        this._installwindows();
    }
    _createwebsite(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " is a programmer function job to create website");
    }
    _fixpc(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " is a programmer function job to fix pc");
    }
    _installwindows(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " is a programmer function job to install windows");
    }
}
exports.Programmer = Programmer;