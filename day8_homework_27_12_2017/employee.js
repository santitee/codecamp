class Employee {
    constructor(firstname, lastname, salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        this._salary = salary; // simulate private variable
    }
    setSalary(salary) {
    if(this._salary > salary){ // simulate public method
        console.log(this.firstname+"'s salary is less than before!!");
        return false;
    }
    else {
        console.log(this.firstname+"'s salary has been set to "+salary);
        return salary;   
    }
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
    };
    getSalary () {  // simulate public method
        return this._salary;
    };

    work(employee) {
        // leave blank for child class to be overidden
    };

    leaveForVacation(year, month, day) {
        //
    }
}

exports.Employee = Employee;