class MobilePhone {
    PhoneCall() 
    SMS()
    InternetSurfing()
}

class iOSPhone extends MobilePhone{
    AppStore()
}

class iPhone8 extends iOSPhone {
    TouchID()
}

class iPhoneX extends iOSPhone {
    FaceID()
}

class SamsungPhone extends MobilePhone {
    UseGearVR()
    TransformtoPC()
    GooglePlay()
}

class SamsungGalaxyNote8 extends SamsungPhone {
    UsePen()
}