--หาว่าพนักงานแต่ละคน มีใครเป็น manager บ้าง
select distinct(e.emp_no), concat(e.first_name," ", e.last_name) as employees_name, emp.emp_no, concat(emp.first_name," ", emp.last_name) as manager_name
from employees as e
left join dept_emp as de on de.emp_no = e.emp_no
left join dept_manager as dm on dm.dept_no = de.dept_no
left join employees as emp on emp.emp_no = dm.emp_no;



--หาว่าพนักงานคนไหน เงินเดือนมากกว่า manager บ้าง

select distinct(e.emp_no), concat(e.first_name," ", e.last_name) as employees_name, se.salary as employees_salary,
emp.emp_no, concat(emp.first_name," ", emp.last_name) as manager_name, s.salary as manager_salary
from employees as e
left join salaries as se on se.emp_no = e.emp_no
left join dept_emp as de on de.emp_no = e.emp_no
left join dept_manager as dm on dm.dept_no = de.dept_no
left join employees as emp on emp.emp_no = dm.emp_no
left join salaries as s on s.emp_no = emp.emp_no
where se.salary > s.salary
order by e.emp_no
limit 100;