--left join

mysql> select courses.id, courses.name, instructors.name
    -> from courses
    -> left join instructors on instructors.id = courses.teach_by
    -> order by courses.id;
+----+-------------------------------------+-----------------------+
| id | name                                | name                  |
+----+-------------------------------------+-----------------------+
|  1 | Cooking                             | Wolfgang Puck         |
|  2 | Acting                              | Samuel L. Jackson     |
|  3 | Chess                               | Garry Kasparov        |
|  4 | Writing                             | Judy Blume            |
|  5 | Conservation                        | Dr. Jane Goodall      |
|  6 | Tennis                              | Serena Williams       |
|  7 | The Art of Performance              | Usher                 |
|  8 | Writing #2                          | James Patterson       |
|  9 | Building a Fashion Brand            | Diane Von Furstenberg |
| 10 | Design and Architecture             | Frank Gehry           |
| 11 | Singing                             | Christina Aguilera    |
| 12 | Jazz                                | Herbie Hancock        |
| 13 | Country Music                       | Reba Mcentire         |
| 14 | Fashion Design                      | Marc Jacobs           |
| 15 | Film Scoring                        | Hans Zimmer           |
| 16 | Comedy                              | Steve Martin          |
| 17 | Writing for Television              | Shonda Rhimes         |
| 18 | Filmmaking                          | Werner Herzog         |
| 19 | Dramatic Writing                    | David Mamet           |
| 20 | Screenwriting                       | Aaron Sorkin          |
| 21 | Electronic Music Production         | Deadmau5              |
| 22 | Cooking #2                          | Gordon Ramsay         |
| 23 | Shooting, Ball Handler, and Scoring | Stephen Curry         |
| 24 | Photography                         | Annie Leibovitz       |
| 25 | Database System Concept             | NULL                  |
| 26 | JavaScript for Beginner             | NULL                  |
| 27 | OWASP Top 10                        | NULL                  |
+----+-------------------------------------+-----------------------+
27 rows in set (0.01 sec)

--aliases

mysql> select courses.id, courses.name as course_name, instructors.name as instructor_name
    -> from courses
    -> left join instructors on instructors.id = courses.teacgh_by
    -> order by courses.id;
ERROR 1054 (42S22): Unknown column 'courses.teacgh_by' in 'on clause'
mysql> select courses.id, courses.name as course_name, instructors.name as instructor_name from courses left join instructors on instructors.id = courses.teach_by order by courses.id;+----+-------------------------------------+-----------------------+
| id | course_name                         | instructor_name       |
+----+-------------------------------------+-----------------------+
|  1 | Cooking                             | Wolfgang Puck         |
|  2 | Acting                              | Samuel L. Jackson     |
|  3 | Chess                               | Garry Kasparov        |
|  4 | Writing                             | Judy Blume            |
|  5 | Conservation                        | Dr. Jane Goodall      |
|  6 | Tennis                              | Serena Williams       |
|  7 | The Art of Performance              | Usher                 |
|  8 | Writing #2                          | James Patterson       |
|  9 | Building a Fashion Brand            | Diane Von Furstenberg |
| 10 | Design and Architecture             | Frank Gehry           |
| 11 | Singing                             | Christina Aguilera    |
| 12 | Jazz                                | Herbie Hancock        |
| 13 | Country Music                       | Reba Mcentire         |
| 14 | Fashion Design                      | Marc Jacobs           |
| 15 | Film Scoring                        | Hans Zimmer           |
| 16 | Comedy                              | Steve Martin          |
| 17 | Writing for Television              | Shonda Rhimes         |
| 18 | Filmmaking                          | Werner Herzog         |
| 19 | Dramatic Writing                    | David Mamet           |
| 20 | Screenwriting                       | Aaron Sorkin          |
| 21 | Electronic Music Production         | Deadmau5              |
| 22 | Cooking #2                          | Gordon Ramsay         |
| 23 | Shooting, Ball Handler, and Scoring | Stephen Curry         |
| 24 | Photography                         | Annie Leibovitz       |
| 25 | Database System Concept             | NULL                  |
| 26 | JavaScript for Beginner             | NULL                  |
| 27 | OWASP Top 10                        | NULL                  |
+----+-------------------------------------+-----------------------+
27 rows in set (0.00 sec)

--aliases(2)
mysql> select c.id, c.name as course_name, i.name as instructor_name from courses as c left join instructors as i on i.id = c.teach_by order by c.id;
+----+-------------------------------------+-----------------------+
| id | course_name                         | instructor_name       |
+----+-------------------------------------+-----------------------+
|  1 | Cooking                             | Wolfgang Puck         |
|  2 | Acting                              | Samuel L. Jackson     |
|  3 | Chess                               | Garry Kasparov        |
|  4 | Writing                             | Judy Blume            |
|  5 | Conservation                        | Dr. Jane Goodall      |
|  6 | Tennis                              | Serena Williams       |
|  7 | The Art of Performance              | Usher                 |
|  8 | Writing #2                          | James Patterson       |
|  9 | Building a Fashion Brand            | Diane Von Furstenberg |
| 10 | Design and Architecture             | Frank Gehry           |
| 11 | Singing                             | Christina Aguilera    |
| 12 | Jazz                                | Herbie Hancock        |
| 13 | Country Music                       | Reba Mcentire         |
| 14 | Fashion Design                      | Marc Jacobs           |
| 15 | Film Scoring                        | Hans Zimmer           |
| 16 | Comedy                              | Steve Martin          |
| 17 | Writing for Television              | Shonda Rhimes         |
| 18 | Filmmaking                          | Werner Herzog         |
| 19 | Dramatic Writing                    | David Mamet           |
| 20 | Screenwriting                       | Aaron Sorkin          |
| 21 | Electronic Music Production         | Deadmau5              |
| 22 | Cooking #2                          | Gordon Ramsay         |
| 23 | Shooting, Ball Handler, and Scoring | Stephen Curry         |
| 24 | Photography                         | Annie Leibovitz       |
| 25 | Database System Concept             | NULL                  |
| 26 | JavaScript for Beginner             | NULL                  |
| 27 | OWASP Top 10                        | NULL                  |
+----+-------------------------------------+-----------------------+
27 rows in set (0.00 sec)

--right join
mysql> select c.id, c.name as course_name, i.name as instructor_name from courses as c right join instructors as i on i.id = c.teach_by order by c.id;
+------+-------------------------------------+-----------------------+
| id   | course_name                         | instructor_name       |
+------+-------------------------------------+-----------------------+
| NULL | NULL                                | Bob Woofward          |
| NULL | NULL                                | Armin Van Buuren      |
| NULL | NULL                                | Alice Waters          |
| NULL | NULL                                | Ron Howard            |
| NULL | NULL                                | Martin Scorsese       |
| NULL | NULL                                | Helen Mirren          |
| NULL | NULL                                | Thomas Keller         |
|    1 | Cooking                             | Wolfgang Puck         |
|    2 | Acting                              | Samuel L. Jackson     |
|    3 | Chess                               | Garry Kasparov        |
|    4 | Writing                             | Judy Blume            |
|    5 | Conservation                        | Dr. Jane Goodall      |
|    6 | Tennis                              | Serena Williams       |
|    7 | The Art of Performance              | Usher                 |
|    8 | Writing #2                          | James Patterson       |
|    9 | Building a Fashion Brand            | Diane Von Furstenberg |
|   10 | Design and Architecture             | Frank Gehry           |
|   11 | Singing                             | Christina Aguilera    |
|   12 | Jazz                                | Herbie Hancock        |
|   13 | Country Music                       | Reba Mcentire         |
|   14 | Fashion Design                      | Marc Jacobs           |
|   15 | Film Scoring                        | Hans Zimmer           |
|   16 | Comedy                              | Steve Martin          |
|   17 | Writing for Television              | Shonda Rhimes         |
|   18 | Filmmaking                          | Werner Herzog         |
|   19 | Dramatic Writing                    | David Mamet           |
|   20 | Screenwriting                       | Aaron Sorkin          |
|   21 | Electronic Music Production         | Deadmau5              |
|   22 | Cooking #2                          | Gordon Ramsay         |
|   23 | Shooting, Ball Handler, and Scoring | Stephen Curry         |
|   24 | Photography                         | Annie Leibovitz       |
+------+-------------------------------------+-----------------------+
31 rows in set (0.00 sec)


--inner join

mysql> select c.id, c.name as course_name, i.name as instructor_name
    -> from courses as c
    -> inner join instructors as i on i.id = c.teach_by
    -> order by c.id;
+----+-------------------------------------+-----------------------+
| id | course_name                         | instructor_name       |
+----+-------------------------------------+-----------------------+
|  1 | Cooking                             | Wolfgang Puck         |
|  2 | Acting                              | Samuel L. Jackson     |
|  3 | Chess                               | Garry Kasparov        |
|  4 | Writing                             | Judy Blume            |
|  5 | Conservation                        | Dr. Jane Goodall      |
|  6 | Tennis                              | Serena Williams       |
|  7 | The Art of Performance              | Usher                 |
|  8 | Writing #2                          | James Patterson       |
|  9 | Building a Fashion Brand            | Diane Von Furstenberg |
| 10 | Design and Architecture             | Frank Gehry           |
| 11 | Singing                             | Christina Aguilera    |
| 12 | Jazz                                | Herbie Hancock        |
| 13 | Country Music                       | Reba Mcentire         |
| 14 | Fashion Design                      | Marc Jacobs           |
| 15 | Film Scoring                        | Hans Zimmer           |
| 16 | Comedy                              | Steve Martin          |
| 17 | Writing for Television              | Shonda Rhimes         |
| 18 | Filmmaking                          | Werner Herzog         |
| 19 | Dramatic Writing                    | David Mamet           |
| 20 | Screenwriting                       | Aaron Sorkin          |
| 21 | Electronic Music Production         | Deadmau5              |
| 22 | Cooking #2                          | Gordon Ramsay         |
| 23 | Shooting, Ball Handler, and Scoring | Stephen Curry         |
| 24 | Photography                         | Annie Leibovitz       |
+----+-------------------------------------+-----------------------+
24 rows in set (0.00 sec)


--left excluding join
mysql> select c.id, c.name as course_name, i.name as instructor_name
    -> from courses as c
    -> left join instructors as i on i.id = c.teach_by
    -> where i.id is null
    -> order by c.id;
+----+-------------------------+-----------------+
| id | course_name             | instructor_name |
+----+-------------------------+-----------------+
| 25 | Database System Concept | NULL            |
| 26 | JavaScript for Beginner | NULL            |
| 27 | OWASP Top 10            | NULL            |
+----+-------------------------+-----------------+
3 rows in set (0.01 sec)

--right excluding join 
mysql> select c.id, c.name as course_name, i.name as instructor_name
    -> from courses as c
    -> right join instructors as i on i.id = c.teach_by
    -> where c.id is null
    -> order by c.id
    -> ;
+------+-------------+------------------+
| id   | course_name | instructor_name  |
+------+-------------+------------------+
| NULL | NULL        | Ron Howard       |
| NULL | NULL        | Martin Scorsese  |
| NULL | NULL        | Helen Mirren     |
| NULL | NULL        | Thomas Keller    |
| NULL | NULL        | Bob Woofward     |
| NULL | NULL        | Armin Van Buuren |
| NULL | NULL        | Alice Waters     |
+------+-------------+------------------+
7 rows in set (0.00 sec)