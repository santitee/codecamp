select distinct c.id, c.name as course_name, i.name as instructors_name, c.price
from courses as c
left join enrolls as e on e.course_id = c.id
left join instructors as i on i.id = c.teach_by
where e.student_id is not null
order by c.id;

+----+-------------------------------------+-----------------------+-------+
| id | course_name                         | instructors_name      | price |
+----+-------------------------------------+-----------------------+-------+
|  1 | Cooking                             | Wolfgang Puck         |    90 |
|  2 | Acting                              | Samuel L. Jackson     |    90 |
|  3 | Chess                               | Garry Kasparov        |    90 |
|  4 | Writing                             | Judy Blume            |    90 |
|  5 | Conservation                        | Dr. Jane Goodall      |    90 |
|  7 | The Art of Performance              | Usher                 |    90 |
|  8 | Writing #2                          | James Patterson       |    90 |
|  9 | Building a Fashion Brand            | Diane Von Furstenberg |    90 |
| 10 | Design and Architecture             | Frank Gehry           |    90 |
| 11 | Singing                             | Christina Aguilera    |    90 |
| 12 | Jazz                                | Herbie Hancock        |    90 |
| 13 | Country Music                       | Reba Mcentire         |    90 |
| 15 | Film Scoring                        | Hans Zimmer           |    90 |
| 16 | Comedy                              | Steve Martin          |    90 |
| 17 | Writing for Television              | Shonda Rhimes         |    90 |
| 18 | Filmmaking                          | Werner Herzog         |    90 |
| 19 | Dramatic Writing                    | David Mamet           |    90 |
| 20 | Screenwriting                       | Aaron Sorkin          |    90 |
| 21 | Electronic Music Production         | Deadmau5              |    90 |
| 22 | Cooking #2                          | Gordon Ramsay         |    90 |
| 23 | Shooting, Ball Handler, and Scoring | Stephen Curry         |    90 |
| 26 | JavaScript for Beginner             | NULL                  |    20 |
| 27 | OWASP Top 10                        | NULL                  |    75 |
+----+-------------------------------------+-----------------------+-------+
23 rows in set (0.00 sec)


select distinct c.id, c.name as course_name, i.name as instructors_name, c.price
from courses as c
left join enrolls as e on e.course_id = c.id
left join instructors as i on i.id = c.teach_by
where e.student_id is null
order by c.id;

+----+-------------------------+------------------+-------+
| id | course_name             | instructors_name | price |
+----+-------------------------+------------------+-------+
|  6 | Tennis                  | Serena Williams  |    90 |
| 14 | Fashion Design          | Marc Jacobs      |    90 |
| 24 | Photography             | Annie Leibovitz  |    90 |
| 25 | Database System Concept | NULL             |    30 |
+----+-------------------------+------------------+-------+
4 rows in set (0.00 sec)


select distinct c.id, c.name as course_name, i.name as instructors_name, c.price
from courses as c
left join enrolls as e on e.course_id = c.id
left join instructors as i on i.id = c.teach_by
where e.student_id is null and c.teach_by is not null
order by c.id;

+----+----------------+------------------+-------+
| id | course_name    | instructors_name | price |
+----+----------------+------------------+-------+
|  6 | Tennis         | Serena Williams  |    90 |
| 14 | Fashion Design | Marc Jacobs      |    90 |
| 24 | Photography    | Annie Leibovitz  |    90 |
+----+----------------+------------------+-------+
3 rows in set (0.01 sec)
