--มีใครบ้างที่ไม่ได้สอนอะไรเลย

mysql> select c.id, c.name as course_name, i.name as instructor_name
    -> from courses as c
    -> right join instructors as i on i.id = c.teach_by
    -> where c.id is null
    -> order by c.id
    -> ;
+------+-------------+------------------+
| id   | course_name | instructor_name  |
+------+-------------+------------------+
| NULL | NULL        | Ron Howard       |
| NULL | NULL        | Martin Scorsese  |
| NULL | NULL        | Helen Mirren     |
| NULL | NULL        | Thomas Keller    |
| NULL | NULL        | Bob Woofward     |
| NULL | NULL        | Armin Van Buuren |
| NULL | NULL        | Alice Waters     |
+------+-------------+------------------+
7 rows in set (0.00 sec)

--มีคอร์สไหนบ้าง ที่ไม่มีคนสอน

mysql> select c.id, c.name as course_name, i.name as instructor_name
    -> from courses as c
    -> left join instructors as i on i.id = c.teach_by
    -> where i.id is null
    -> order by c.id;
+----+-------------------------+-----------------+
| id | course_name             | instructor_name |
+----+-------------------------+-----------------+
| 25 | Database System Concept | NULL            |
| 26 | JavaScript for Beginner | NULL            |
| 27 | OWASP Top 10            | NULL            |
+----+-------------------------+-----------------+
3 rows in set (0.01 sec)

