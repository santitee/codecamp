-- เพิ่ม Student ลงไป 10 คน

mysql> insert into students(name) values
    -> ('Steve Vai'),
    -> ('Jeff Beck'),
    -> ('Eric Clapton'),
    -> ('Kirk Hammett'),
    -> ('James Hetfield'),
    -> ('David Sanborn'),
    -> ('John Scofield'),
    -> ('Van Halen'),
    -> ('Scott Henderson'),
    -> ('Carlos Santana');
Query OK, 10 rows affected (0.01 sec)
Records: 10  Duplicates: 0  Warnings: 0

--ให้ Student  ลงเรียนแต่ละวิชาที่แตกต่างกัน(อาจจะซ้ำกันบ้าง)

insert into enrolls(student_id, course_id) values
(1, 3),
(2, 9),
(3, 10),
(4, 11),
(5, 12),
(6, 22),
(7, 26),
(8, 1),
(9, 2),
(10, 4),
(1, 4),
(2, 8),
(2, 22),
(3, 7),
(4, 1),
(4, 21),
(5, 13),
(6, 15),
(6, 17),
(7, 2),
(1, 5),
(7, 18),
(2, 25),
(3, 27),
(6, 4),
(7, 23),
(9, 16),
(8, 13),
(3, 19),
(4, 20),
(5, 21),
(6, 12),
(9, 26),
(6, 8),
(9, 7),
(4, 18),
(6, 2),
(3, 17);
Query OK, 40 rows affected (0.01 sec)
Records: 40  Duplicates: 0  Warnings: 0

--left join
select distinct c.id, c.name as course_name
from courses as c
left join enrolls as e on e.course_id = c.id
where e.student_id is not null
order by c.id;

mysql> select distinct c.id, c.name as course_name
    -> from courses as c
    -> left join enrolls as e on e.course_id = c.id
    -> where e.student_id is not null
    -> order by c.id;
+----+-------------------------------------+
| id | course_name                         |
+----+-------------------------------------+
|  1 | Cooking                             |
|  2 | Acting                              |
|  3 | Chess                               |
|  4 | Writing                             |
|  5 | Conservation                        |
|  7 | The Art of Performance              |
|  8 | Writing #2                          |
|  9 | Building a Fashion Brand            |
| 10 | Design and Architecture             |
| 11 | Singing                             |
| 12 | Jazz                                |
| 13 | Country Music                       |
| 15 | Film Scoring                        |
| 16 | Comedy                              |
| 17 | Writing for Television              |
| 18 | Filmmaking                          |
| 19 | Dramatic Writing                    |
| 20 | Screenwriting                       |
| 21 | Electronic Music Production         |
| 22 | Cooking #2                          |
| 23 | Shooting, Ball Handler, and Scoring |
| 25 | Database System Concept             |
| 26 | JavaScript for Beginner             |
| 27 | OWASP Top 10                        |
+----+-------------------------------------+
24 rows in set (0.00 sec)