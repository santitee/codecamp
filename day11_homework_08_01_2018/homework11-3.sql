use books_store;

mysql> alter table books
    -> modify column isbn_id varchar(13);
Query OK, 0 rows affected (0.03 sec)
Records: 0  Duplicates: 0  Warnings: 0

insert into books(isbn_id, bookname, price) values
(1143849501112,'Javascript', 300),
(1489088888135,'Python for beginer', 250),
(1946638465247,'Learning Django Web Development', 580),
(5786931253490,'Learning Full Stack Development', 800),
(9781786893349,'Mastering Node.js', 490),
(9788756683980,'Learning Reactjs', 550),
(9781785881659,'Mastering JavaScript Single', 690),
(1617290939030,'Node.js in Practice', 490),
(9781783987992,'Mastering JavaScript Design Patterns', 450),
(9781784395788,'Node.js By Example', 390);


mysql> select * from books;
+---------------+--------------------------------------+-------+---------------------+
| isbn_id       | bookname                             | price | created_at          |
+---------------+--------------------------------------+-------+---------------------+
| 1143849501112 | Javascript                           |   300 | 2018-01-08 17:46:39 |
| 1489088888135 | Python for beginer                   |   250 | 2018-01-08 17:46:39 |
| 1617290939030 | Node.js in Practice                  |   490 | 2018-01-08 17:46:39 |
| 1946638465247 | Learning Django Web Development      |   580 | 2018-01-08 17:46:39 |
| 5786931253490 | Learning Full Stack Development      |   800 | 2018-01-08 17:46:39 |
| 9781783987992 | Mastering JavaScript Design Patterns |   450 | 2018-01-08 17:46:39 |
| 9781784395788 | Node.js By Example                   |   390 | 2018-01-08 17:46:39 |
| 9781785881659 | Mastering JavaScript Single          |   690 | 2018-01-08 17:46:39 |
| 9781786893349 | Mastering Node.js                    |   490 | 2018-01-08 17:46:39 |
| 9788756683980 | Learning Reactjs                     |   550 | 2018-01-08 17:46:39 |
+---------------+--------------------------------------+-------+---------------------+
10 rows in set (0.00 sec)

mysql> select * from books
    -> where bookname like '%script%';
+---------------+--------------------------------------+-------+---------------------+
| isbn_id       | bookname                             | price | created_at          |
+---------------+--------------------------------------+-------+---------------------+
| 1143849501112 | Javascript                           |   300 | 2018-01-08 17:46:39 |
| 9781783987992 | Mastering JavaScript Design Patterns |   450 | 2018-01-08 17:46:39 |
| 9781785881659 | Mastering JavaScript Single          |   690 | 2018-01-08 17:46:39 |
+---------------+--------------------------------------+-------+---------------------+
3 rows in set (0.01 sec)

mysql> select * from books
    -> where bookname like '%a%'
    -> limit 4;
+---------------+---------------------------------+-------+---------------------+
| isbn_id       | bookname                        | price | created_at          |
+---------------+---------------------------------+-------+---------------------+
| 1143849501112 | Javascript                      |   300 | 2018-01-08 17:46:39 |
| 1617290939030 | Node.js in Practice             |   490 | 2018-01-08 17:46:39 |
| 1946638465247 | Learning Django Web Development |   580 | 2018-01-08 17:46:39 |
| 5786931253490 | Learning Full Stack Development |   800 | 2018-01-08 17:46:39 |
+---------------+---------------------------------+-------+---------------------+
4 rows in set (0.00 sec)

mysql> describe orders;
+----------------+-------------+------+-----+-------------------+----------------+
| Field          | Type        | Null | Key | Default           | Extra          |
+----------------+-------------+------+-----+-------------------+----------------+
| id             | int(11)     | NO   | PRI | NULL              | auto_increment |
| salecode_id    | int(11)     | YES  |     | NULL              |                |
| isbn_id        | varchar(13) | YES  |     | NULL              |                |
| quantity       | int(11)     | YES  |     | NULL              |                |
| sale_create_at | timestamp   | NO   |     | CURRENT_TIMESTAMP |                |
+----------------+-------------+------+-----+-------------------+----------------+
5 rows in set (0.01 sec)


mysql> describe orders;
+----------------+-------------+------+-----+-------------------+----------------+
| Field          | Type        | Null | Key | Default           | Extra          |
+----------------+-------------+------+-----+-------------------+----------------+
| id             | int(11)     | NO   | PRI | NULL              | auto_increment |
| salecode_id    | int(11)     | YES  |     | NULL              |                |
| isbn_id        | varchar(13) | YES  |     | NULL              |                |
| quantity       | int(11)     | YES  |     | NULL              |                |
| sale_create_at | timestamp   | NO   |     | CURRENT_TIMESTAMP |                |
+----------------+-------------+------+-----+-------------------+----------------+
5 rows in set (0.01 sec)

insert into orders(salecode_id, isbn_id, quantity,price_per_unit) values
(1,1143849501112, 1, 300),
(8,1489088888135, 1, 250),
(3,9781784395788, 1, 390),
(4,1946638465247, 1, 580),
(6,5786931253490, 1, 800),
(7,5786931253490, 2, 800),
(8,9781786893349, 1, 490),
(1,9788756683980, 1, 550),
(2,9781785881659, 2, 690),
(1,1617290939030, 2, 490),
(9,9781783987992, 1, 450),
(10,9781784395788, 1, 390),
(7,1143849501112, 1, 300),
(2,1489088888135, 1, 250),
(3,1946638465247, 1, 580),
(6,5786931253490, 1, 800),
(8,9781786893349, 1, 490),
(8,9788756683980, 1, 550),
(7,9781785881659, 1, 690),
(10,1617290939030, 1, 490),
(4,9781783987992, 1, 450),
(3,9781784395788, 1, 390);

mysql> select count(*) from orders;
+----------+
| count(*) |
+----------+
|       22 |
+----------+
1 row in set (0.00 sec)


mysql> select sum(quantity) from orders;
+---------------+
| sum(quantity) |
+---------------+
|            25 |
+---------------+
1 row in set (0.01 sec)

mysql> select distinct isbn_id from orders;
+---------------+
| isbn_id       |
+---------------+
| 1143849501112 |
| 1489088888135 |
| 9781784395788 |
| 1946638465247 |
| 5786931253490 |
| 9781786893349 |
| 9788756683980 |
| 9781785881659 |
| 1617290939030 |
| 9781783987992 |
+---------------+
10 rows in set (0.00 sec)

mysql> alter table orders add column price_per_unit int;
Query OK, 0 rows affected (0.13 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select sum(quantity * price_per_unit) as Total_Sale_amount from orders;
+-------------------+
| Total_Sale_amount |
+-------------------+
|             13150 |
+-------------------+
1 row in set (0.01 sec)


