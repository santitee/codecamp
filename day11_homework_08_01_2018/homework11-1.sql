CREATE DATABASE books_store;

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| book_store         |
| books_store        |
| codecamp           |
| db1                |
| mysql              |
| performance_schema |
| sys                |
+--------------------+

USE books_store;

mysql> use books_store;
Database changed


CREATE TABLE employees (
	id int auto_increment primary key,
	firstname varchar(255),
	lastname varchar(255),
	age	int not null,
	created_at timestamp default current_timestamp
);

CREATE TABLE books (
	isbn_id int primary key,
	bookname varchar(255),
	price int default null,
	created_at timestamp default current_timestamp
);

create table orders (
	id int auto_increment primary key,
	salecode_id int,
	isbn_id int,
	quantity int,
	sale_create_at timestamp default current_timestamp
);

mysql> show tables;
+-----------------------+
| Tables_in_books_store |
+-----------------------+
| books                 |
| employees             |
| orders                |
+-----------------------+
3 rows in set (0.00 sec)