use books_store;

insert into employees (firstname, lastname, age) values 
('Asanee','Choti', 58),
('Wasan','Choti', 56),
('Robbie','William', 46),
('Cristina','Aqela', 50),
('Jacky','Chan', 58),
('Mai','Davika', 22),
('Toy','Playground', 18),
('Addy','Autobarn', 49),
('James','Jirayu', 25),
('Boy','Trai',26);

mysql> describe employees;
+------------+--------------+------+-----+-------------------+----------------+
| Field      | Type         | Null | Key | Default           | Extra          |
+------------+--------------+------+-----+-------------------+----------------+
| id         | int(11)      | NO   | PRI | NULL              | auto_increment |
| firstname  | varchar(255) | YES  |     | NULL              |                |
| lastname   | varchar(255) | YES  |     | NULL              |                |
| age        | int(11)      | NO   |     | NULL              |                |
| created_at | timestamp    | NO   |     | CURRENT_TIMESTAMP |                |
+------------+--------------+------+-----+-------------------+----------------+
5 rows in set (0.00 sec)

mysql> select count(*) from employees;
+----------+
| count(*) |
+----------+
|       10 |
+----------+
1 row in set (0.00 sec)

delete from employees 
where id = 5;

select * from emplyees;
mysql> select count(*) from employees;
+----------+
| count(*) |
+----------+
|        9 |
+----------+
1 row in set (0.00 sec)

mysql> select * from employees;
+----+-----------+------------+-----+---------------------+
| id | firstname | lastname   | age | created_at          |
+----+-----------+------------+-----+---------------------+
|  1 | Asanee    | Choti      |  58 | 2018-01-08 16:47:45 |
|  2 | Wasan     | Choti      |  56 | 2018-01-08 16:47:45 |
|  3 | Robbie    | William    |  46 | 2018-01-08 16:47:45 |
|  4 | Cristina  | Aqela      |  50 | 2018-01-08 16:47:45 |
|  6 | Mai       | Davika     |  22 | 2018-01-08 16:47:45 |
|  7 | Toy       | Playground |  18 | 2018-01-08 16:47:45 |
|  8 | Addy      | Autobarn   |  49 | 2018-01-08 16:47:45 |
|  9 | James     | Jirayu     |  25 | 2018-01-08 16:47:45 |
| 10 | Boy       | Trai       |  26 | 2018-01-08 16:47:45 |
+----+-----------+------------+-----+---------------------+
9 rows in set (0.00 sec)

mysql> alter table employees
    -> add column address varchar(255);
Query OK, 0 rows affected (0.12 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe employees;
+------------+--------------+------+-----+-------------------+----------------+
| Field      | Type         | Null | Key | Default           | Extra          |
+------------+--------------+------+-----+-------------------+----------------+
| id         | int(11)      | NO   | PRI | NULL              | auto_increment |
| firstname  | varchar(255) | YES  |     | NULL              |                |
| lastname   | varchar(255) | YES  |     | NULL              |                |
| age        | int(11)      | NO   |     | NULL              |                |
| created_at | timestamp    | NO   |     | CURRENT_TIMESTAMP |                |
| address    | varchar(255) | YES  |     | NULL              |                |
+------------+--------------+------+-----+-------------------+----------------+
6 rows in set (0.00 sec)

mysql> update employees
    -> set address = '85/190'
    -> where id = 8;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from employees;
+----+-----------+------------+-----+---------------------+---------+
| id | firstname | lastname   | age | created_at          | address |
+----+-----------+------------+-----+---------------------+---------+
|  1 | Asanee    | Choti      |  58 | 2018-01-08 16:47:45 | NULL    |
|  2 | Wasan     | Choti      |  56 | 2018-01-08 16:47:45 | NULL    |
|  3 | Robbie    | William    |  46 | 2018-01-08 16:47:45 | NULL    |
|  4 | Cristina  | Aqela      |  50 | 2018-01-08 16:47:45 | NULL    |
|  6 | Mai       | Davika     |  22 | 2018-01-08 16:47:45 | NULL    |
|  7 | Toy       | Playground |  18 | 2018-01-08 16:47:45 | NULL    |
|  8 | Addy      | Autobarn   |  49 | 2018-01-08 16:47:45 | 85/190  |
|  9 | James     | Jirayu     |  25 | 2018-01-08 16:47:45 | NULL    |
| 10 | Boy       | Trai       |  26 | 2018-01-08 16:47:45 | NULL    |
+----+-----------+------------+-----+---------------------+---------+
9 rows in set (0.00 sec)

mysql> select count(*) from employees;
+----------+
| count(*) |
+----------+
|        9 |
+----------+
1 row in set (0.00 sec)

mysql> select * from employees where age < 20;
+----+-----------+------------+-----+---------------------+---------+
| id | firstname | lastname   | age | created_at          | address |
+----+-----------+------------+-----+---------------------+---------+
|  7 | Toy       | Playground |  18 | 2018-01-08 16:47:45 | NULL    |
+----+-----------+------------+-----+---------------------+---------+
1 row in set (0.02 sec)

