let fs = require('fs');

function readHead() {
    return new Promise(function (resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, head) {
            if (err)
                reject(err);
            else
                resolve(head);
        });
    });
}

function readBody() {
    return new Promise(function (resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, body) {
            if (err)
                reject(err);
            else
                resolve(body);
        });
    });
}

function readLeg() {
    return new Promise(function (resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, leg) {
            if (err)
                reject(err);
            else
                resolve(leg);
        });
    });
}

function readFeet() {
    return new Promise(function (resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, feet) {
            if (err)
                reject(err);
            else
                resolve(feet);
        });
    });
}

//function writFile
function writeRobot(result) {
        fs.writeFile('robot.txt', result, 'utf8', function (err){           
        });
};


async function createRobotFile() {
    try {
        let dataHead = await readHead();
        let dataBody = await readBody();
        let dataLeg  = await readLeg();
        let dataFeet = await readFeet();
        let result = dataHead+'\n'+dataBody+'\n'+dataLeg+'\n'+dataFeet;
        writeRobot(result);
        console.log(result);
    } catch (error) {
        console.error(error);
    }
}
createRobotFile();
