let fs = require('fs');
//read file by filename
let readFilePromise = [
    readFileByFileName('head.txt'),
    readFileByFileName('body.txt'),
    readFileByFileName('leg.txt'),
    readFileByFileName('foot.txt'),
]

let pHead = new Promise(function(resolve, reject) {
    fs.readFile('head.txt', 'utf8', function(err, head) { 
        if (err)
            reject(err);
        else
        resolve(head);
    });
});

let pBody = new Promise(function(resolve, reject) {
    fs.readFile('body.txt', 'utf8', function(err, body) { 
        if (err)
            reject(err);
        else
        resolve(body); 
    });
});

let pLeg = new Promise(function(resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function(err, leg) { 
        if (err)
            reject(err);
        else
        resolve(leg); 
    });
});

let pFeet = new Promise(function(resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function(err, feet) { 
        if (err)
            reject(err);
        else
        resolve(feet);
        //console.log(pFeet); 
    });
});

//let robot =  result.join();

Promise.all([pHead, pBody, pLeg, pFeet])
.then(function(result){
    // console.log('All completed!: ', result); // result = ['one','two','three']
    let robot = result.join('\n');//data join()
    fs.writeFile('robot.txt',robot, 'utf8', function(err,data){
    })
    console.log(robot)
}).catch(function(error){
    console.error("There's an error", error);
})



