const fs = require('fs');

function calculate(type, a, b){
    if (type == 'add')
        return add(a,b);
    else if (type == 'substract')
        return substract(a,b);
    else if (type== 'multiply')
        return multiply(a,b);
    else
        return divideBy(a,b);
    
}

function saveUserDatabase(user, filename, callback) {
    let json_string = JSON.stringify(user);
    if (json_string.length > 0) {
      fs.writeFile(filename, json_string, 'utf8', function(err) {
        if (err)
          callback(err);
        else
          callback(null);
        }); 
    }
}

function veryLongTimeFunction(callback){
    mySetTimeout(function(){
        callback("finished");
    }, 5000);
}

function mySetTimeout(callbackFunctiion, time){
    mySetTimeout(function(){
        callback("finished");
    }, 5000);
}

exports.calculate = calculate;
exports.saveUserDatabase = saveUserDatabase;
exports.veryLongTimeFunction = veryLongTimeFunction;
exports.mySetTimeout = mySetTimeout;
