const assert = require('assert');
const lab6 = require('./lab6_1.js');
const fs = require('fs');
const sinon = require('sinon');

function saveUserDatabase(user, filename, callback) {
    let json_string = JSON.stringify(user);
    if (json_string.length > 0) {
        fs.writeFile(filename, json_string, 'utf8', function(err) {
        if (err)
            callback(err);
        else
            callback(null);
        }); 
    }
}
    

describe('tdd lab -spy', function(){
    it('should write JSON string into output6_1.txt. check with spy', function() { 
        const save = sinon.spy(fs,"writeFile");
        const user = {
        "firstname" : "Somchai",
        "lastname" : "Sudlor" };
        let dummyCallbackFunction = function(err){}; 
        lab6.saveUserDatabase(user, "output6_1.txt",dummyCallbackFunction);
        save.restore(); 
        sinon.assert.calledOnce(save); 
        sinon.assert.calledWith(save,'output6_1.txt');
    });
});

