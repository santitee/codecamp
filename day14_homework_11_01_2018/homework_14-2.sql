create database books_hw;
use books_hw;
create table employee (
  id int primary key,
  department_id int,
  name varchar(255) default null,
  address varchar(255) default null,
  salary int default null,
  foreign key(department_id) references department (id)
);

create table department (
  id int auto_increment primary key,
  name varchar(255),
  budget int default null
);

create table customer (
  id int auto_increment primary key,
  name varchar(255) default null,
  address varchar(255) default null
);

create table supplier (
  id int auto_increment primary key,
  name varchar(255) default null,
  address varchar(255) default null,
  phone_number int(10) default null
);

create table product (
  id int auto_increment primary key,
  name varchar(255) not null,
  description varchar(255) default null,
  price int default 0,
  quantity int default 0
);

create table order_items (
  order_id INT
  product_id int   
  amount int default null,
  discount int default null,
  foreign key(product_id) references product(id),
  foreign key(order_id) references orders(id)
);

create table orders (
  id int not null auto_increment primary key,
  customer_id int,
  date timestamp default current_timestamp,
  foreign key(customer_id) references customer (id)
);

create table employees_orders (
  employees_id int default null,
  order_id int default null,
  foreign key(employees_id) references employee (id),
  foreign key(order_id) references orders (id)
);
