let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];

let result = [];

for(let i = 0; i < employees.length; i++ ){
    let employee = employees[i];
    let obj = {};
    //console.log()
    //console.log(employee);
    for( let key = 0; key < employee.length; key++){
        //obj[key] = employee[key][i];
        obj[fields[key]] = employee[key];
        //console.log(fields[key]);
        //console.log(employee[key]);        
    }
    //console.log(obj);
    result.push(obj);
};
console.log(result);