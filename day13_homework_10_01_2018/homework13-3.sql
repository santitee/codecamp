
---------------111111111111111------------------------------------------------
mysql> select @@GLOBAL.tx_isolation, @@tx_isolation;
+-----------------------+-----------------+
| @@GLOBAL.tx_isolation | @@tx_isolation  |
+-----------------------+-----------------+
| REPEATABLE-READ       | REPEATABLE-READ |
+-----------------------+-----------------+
1 row in set, 2 warnings (0.00 sec)

mysql> begin;
Query OK, 0 rows affected (0.01 sec)

mysql> select balance from accounts where id = 1;
+---------+
| balance |
+---------+
|   10000 |
+---------+
1 row in set (0.00 sec)

mysql> select balance from accounts where id = 1;
+---------+
| balance |
+---------+
|   10000 |
+---------+
1 row in set (0.00 sec)

mysql> commit;
Query OK, 0 rows affected (0.00 sec)

mysql> begin;
Query OK, 0 rows affected (0.00 sec)

mysql> select balance from accounts where id = 1;
+---------+
| balance |
+---------+
|   10000 |
+---------+
1 row in set (0.00 sec)

mysql> select balance from accounts where id = 1;
+---------+
| balance |
+---------+
|   10000 |
+---------+
1 row in set (0.00 sec)

mysql> 

---------------22222222222222222222------------------------------------------------
