select t.title, avg(s.salary) as avg_title
from titles as t 
left join salaries as s on s.emp_no = t.emp_no
group by t.title;

+--------------------+------------+
| title              | avg_title  |
+--------------------+------------+
| Assistant Engineer | 59304.9863 |
| Engineer           | 59508.0397 |
| Manager            | 66924.2706 |
| Senior Engineer    | 60543.2191 |
| Senior Staff       | 70470.8353 |
| Staff              | 69309.1023 |
| Technique Leader   | 59294.3742 |
+--------------------+------------+
7 rows in set (8.34 sec)
-------------------------

create view title_salary as
select t.title, max(s.salary) as max_salary, min(s.salary) as min_salary
from titles as t 
left join salaries as s on s.emp_no = t.emp_no
group by t.title;

create view max_sal as
select max(s.salary) as max_salary
from salaries as s

create view min_sal as 
select min(s.salary) as min_salary
from salaries as s

select ts.title, ts.max_salary as max_salary, '' as min_salary
from title_salary as ts
where ts.max_salary = (select max_salary from max_sal) 
union
select ts.title, '', ts.min_salary as min_salary
from title_salary as ts
where ts.min_salary = (select min_salary from min_sal);


+------------------+------------+------------+
| title            | max_salary | min_salary |
+------------------+------------+------------+
| Staff            | 158220     |            |
| Senior Staff     | 158220     |            |
| Technique Leader |            | 38623      |
+------------------+------------+------------+
3 rows in set (17.42 sec)

-------

select t.title, count(*) as count_employees
from titles as t
left join employees as e on e.emp_no = t.emp_no
group by t.title;


+--------------------+-----------------+
| title              | count_employees |
+--------------------+-----------------+
| Assistant Engineer |           15128 |
| Engineer           |          115003 |
| Manager            |              24 |
| Senior Engineer    |           97750 |
| Senior Staff       |           92853 |
| Staff              |          107391 |
| Technique Leader   |           15159 |
+--------------------+-----------------+
7 rows in set (1.06 sec)