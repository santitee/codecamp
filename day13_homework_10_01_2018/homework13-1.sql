create view course_prices as
select distinct(c.id), c.name, c.price, count(e.student_id) as count, c.price * count(e.student_id) as amount
from courses as c
left join enrolls as e on e.course_id = c.id
where e.student_id is not null
group by c.id
union select '', '', '', '', sum(cp.amount) as total_amount
from course_prices as cp;

+----+-------------------------------------+-------+-------+--------+
| 1  | Cooking                             | 120   | 2     |    240 |
| 2  | Acting                              | 90    | 3     |    270 |
| 3  | Chess                               | 150   | 1     |    150 |
| 4  | Writing                             | 90    | 3     |    270 |
| 5  | Conservation                        | 160   | 1     |    160 |
| 7  | The Art of Performance              | 180   | 2     |    360 |
| 8  | Writing #2                          | 90    | 2     |    180 |
| 9  | Building a Fashion Brand            | 190   | 1     |    190 |
| 10 | Design and Architecture             | 90    | 1     |     90 |
| 11 | Singing                             | 90    | 1     |     90 |
| 12 | Jazz                                | 200   | 2     |    400 |
| 13 | Country Music                       | 90    | 2     |    180 |
| 15 | Film Scoring                        | 90    | 1     |     90 |
| 16 | Comedy                              | 90    | 1     |     90 |
| 17 | Writing for Television              | 90    | 2     |    180 |
| 18 | Filmmaking                          | 90    | 3     |    270 |
| 19 | Dramatic Writing                    | 90    | 1     |     90 |
| 20 | Screenwriting                       | 90    | 1     |     90 |
| 21 | Electronic Music Production         | 90    | 3     |    270 |
| 22 | Cooking #2                          | 90    | 2     |    180 |
| 23 | Shooting, Ball Handler, and Scoring | 90    | 1     |     90 |
| 26 | JavaScript for Beginner             | 20    | 2     |     40 |
| 27 | OWASP Top 10                        | 75    | 1     |     75 |
|    |                                     |       |       |   4045 |
+----+-------------------------------------+-------+-------+--------+
24 rows in set (0.00 sec)

--
select e.student_id, s.name, sum(c.price) as total_payment
from enrolls as e
left join students as s on s.id = e.student_id
left join courses as c on  c.id = e.course_id
where e.student_id is not null
group by e.student_id;

+------------+-----------------+---------------+
| student_id | name            | total_payment |
+------------+-----------------+---------------+
|          1 | Steve Vai       |           490 |
|          2 | Jeff Beck       |           460 |
|          3 | Eric Clapton    |           525 |
|          4 | Kirk Hammett    |           480 |
|          5 | James Hetfield  |           380 |
|          6 | David Sanborn   |           740 |
|          7 | John Scofield   |           290 |
|          8 | Van Halen       |           210 |
|          9 | Scott Henderson |           380 |
|         10 | Carlos Santana  |            90 |
+------------+-----------------+---------------+
10 rows in set (0.00 sec)