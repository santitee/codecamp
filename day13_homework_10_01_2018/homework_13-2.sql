select e.student_id, s.name, count(*),sum(c.price) as total_payment
from enrolls as e
left join students as s on s.id = e.student_id
left join courses as c on  c.id = e.course_id
where e.student_id is not null
group by e.student_id;

--
select e.student_id, s.name, c.name as course_name, c.price as max_price_course
from enrolls as e
left join students as s on s.id = e.student_id
left join courses as c on c.id = e.course_id
where e.student_id is not null and price = (select max(c.price) from courses as c)
group by e.student_id, c.name;

--
create view cp_max as
select e.student_id, s.name, c.name as course_name, c.price as max_price_course
from enrolls as e
left join students as s on s.id = e.student_id
left join courses as c on c.id = e.course_id
where e.student_id is not null
group by e.student_id, c.name;

create view student_max_price as 
select cpm.student_id, cpm.name, max(cpm.max_price_course) as max_price_course
from cp_max as cpm
group by cpm.student_id;

select e.student_id, s.name, c.name as course_name, smp.max_price_course
from enrolls as e
left join students as s on s.id = e.student_id
left join courses as c on c.id = e.course_id
left join student_max_price as smp on e.student_id = smp.student_id
where e.student_id is not null and c.price = smp.max_price_course
group by e.student_id, c.name;