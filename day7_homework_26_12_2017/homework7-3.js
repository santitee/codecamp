let input1 = [1,2,3];
let input2 = {a:1,b:2};
let input3 = [1,2,{a:1,b:2}];
let input4 =[1,2,{a:1,b:{c:3,d:4}}];

function cloneObject(data) {
    let temp ;
    if(Array.isArray(data) === true ) {
        //Array
        temp = [];
        for(let key in data){
            if(typeof(data[key]) === 'number') {
                temp[key] = data[key];
            }
            else { temp[key] = {}; 
                for( let key2 in data[key]) {
                    if(typeof(data[key][key2]) === 'number') {
                        temp[key][key2] = data[key][key2];
                    }
                    else { temp[key][key2] = {}; 
                        for( let key3 in data[key][key2]) {
                            if(typeof(data[key][key2][key3]) === 'number') {
                                temp[key][key2][key3] = data[key][key2][key3];   
                            }
                        }
                    }
                }
            } 
        }
    } 
    else {
        //Object
        temp = {};
        for( let key in data){
            temp[key] = data[key];
        }  
    }
    // console.log(temp)
    return temp;
}

let clone = cloneObject(input4);

clone[0] = 77;
console.log('This is data', input4);
console.log('This is clone data',clone);

exports.cloneObject = cloneObject