const Koa = require('koa');
const app = new Koa();
const logger = require('koa-logger');
const winston = require('winston');
const winLog = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' }) ]
  });
  winLog.add(new winston.transports.Console({ format: winston.format.simple()
  }));
  winLog.info('This is info log');
  winLog.error('This is error log');

app.use(async (ctx, next) => {
  ctx.body = 'Hello World';
  await next(); 
});

app.use(async (ctx, next) => {
    try {
      await next();
        someUnknownFunction(); 
    } catch (err) {
      ctx.status = 400
      ctx.body = `Uh-oh: ${err.message}`
      console.log('Error handler:', err.message)
      winLog.info('This is info log');
      winLog.error('This is error log');
  } })

app.use(logger());
app.listen(3000);

