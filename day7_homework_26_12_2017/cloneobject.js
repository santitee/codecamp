function copy(mainObj) {
    let objCopy = {}; // objCopy will store a copy of the mainObj
    let key;
  
    for (key in mainObj) {
      objCopy.a = 0;
      objCopy[key] = mainObj[key]; // copies each property to the objCopy object
    }
    return objCopy;
    console.log(copy(objCopy))
  }
  
  const mainObj = {
    a: 2,
    b: 5,
    c: {
      x: 7,
      y: 4,
    },
  }
  console.log(copy(mainObj)
);