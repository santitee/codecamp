const assert = require('assert');
const cloneObject = require('./homework7-3.js');

let input1 = [1,2,3];
let input2 = {a:1,b:2};
let input3 = [1,2,{a:1,b:2}];
let input4 = [1,2,{a:1,b:{c:3,d:4}}];

// let input5 = [1,2555,38980890];
// let input6 = {a:17899,b:298774};
// let input7 = [1,2099,{a:1923345,b:2}];
// let input8 = [1,2,{a:999,b:{c:3,d:4444}}];

let input5 = [1,2,3];
let input6 = {a:1,b:2};
let input7 = [1,2,{a:1,b:2}];
let input8 = [1,2,{a:1,b:{c:3,d:4}}];

describe('#cloneObject()', function() {
    it('should have clone object key stucture as input 1', function() {
        assert.deepStrictEqual(cloneObject.cloneObject(input5), input1, "check input is not same as clone object")
    });
    it('should have clone object key stucture as input 2', function() {
        assert.deepStrictEqual(cloneObject.cloneObject(input6), input2, "check input is not same as clone object")
    });
    it('should have clone object key stucture as input 3', function() {
        assert.deepStrictEqual(cloneObject.cloneObject(input7), input3, "check input is not same as clone object")
    });
    it('should have clone object key stucture as input 4', function() {
        assert.deepStrictEqual(cloneObject.cloneObject(input8), input4, "check input is not same as clone object")
    });
});
