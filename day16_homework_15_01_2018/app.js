const Koa = require('koa');
const render = require('koa-ejs');
const path = require('path');
const app = new Koa();
// const db = require('./lib/db')();
const otherMiddleware = require('./controller/routes.js')(app);

// const otherMiddleware = require('./lib/db.js')(conn);

render(app,{
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});


