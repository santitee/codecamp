const {db} = require('../lib/db.js');

    async function getUser () {
    //query database
    const rows = await db.execute("SELECT * FROM user ");
    return rows;
    
    async function save()
    if (!this.id) {
        const result = await this._db.execute(`
        insert into users (
            first_name, last_name
        ) values (
            ?, ?
        )
        `,  [this.firstName, this.lastName])
        this.id = result.insertID
        return
    };
    return this._db.execute(`
    update users
    set
        first_name = ?,
        last_name = ?
    where id = ?
    `, [this.firstName, this.lastName, this.id])
}

exports.getUser = getUser;