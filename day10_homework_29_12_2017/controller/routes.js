const logger = require('koa-logger');
const models = require('../models/homework10-1.js')

module.exports = function (app) {
    app.use(async (ctx, next) => {
        try {
            // query database
            const rows = await models.getUser();
            await ctx.render('user', {
                "data": rows
            })
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });
    app.use(logger());
    app.listen(3000);
}
