const {Employee} = require('./employee.js');

class Programmer extends Employee {
    constructor( id, firstname, lastname, salary, role,type) {
        super(id, firstname, lastname, salary);
        this.role = role;
        this.type = type;
    }
    work() {  // simulate public method
        this._createwebsite();
        this._fixpc();
        this._installwindows();
    }
    _createwebsite() { // simulate private method
        console.log(this.firstname + " is a programmer function job to create website.");
    }
    _fixpc() { // simulate private method
        console.log(this.firstname + " is a programmer function job to fix pc.");
    }
    _installwindows() { // simulate private method
        console.log(this.firstname + " is a programmer function job to install windows.");
    }
}
exports.Programmer = Programmer;