const EventEmitter = require('events');
const myEmitter = new EventEmitter();

// myEmitter.on('myEvent', () => {
//     console.log('an event1 occurred!');
// });
// myEmitter.emit('myEvent');
//------------------------------------------------
myEmitter.on('myEvent', () => {
    console.log('an event1 occurred!');
    });
myEmitter.emit('myEvent');
myEmitter.once('myEvent', () => {
    console.log('an event2 occurred!');
    });
myEmitter.emit('myEvent');
myEmitter.on('myEvent', () => {
    console.log('an event3 occurred!');
    });
myEmitter.emit('myEvent');
// //--------------------------------------------------

// myEmitter.on('myEvent2', (a,b) => {
//         console.log('an event occurred! '+a+' '+b);
//     });
// myEmitter.emit('myEvent2',1,2);
// myEmitter.emit('myEvent2',1,2);
// //--------------------------------------------------
// myEmitter.once('myEvent2', (a,b) => {
//     console.log('an event occurred! '+a+' '+b);
//     });
//     myEmitter.emit('myEvent2',1,2);
//     myEmitter.emit('myEvent2',1,2);