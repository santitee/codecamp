const fs = require('fs');
const {Employee} = require('./employee.js');
const {Programmer} = require('./programmer.js');
const {OfficeCleaner} = require('./officecleaner.js');
const EventEmitter = require('events');

class CEO extends Employee {
    constructor(id,firstname, lastname, salary, role, dressCode) {
        super(id,firstname, lastname, salary);
        this.role = role;
        this.dressCode = dressCode;
        let self = this;
        // this.employeesRaw = [];
        // this.employees = []; 
    }
    getSalary() {  // simulate public method
        return super.getSalary() * 2;
    }
    work(employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)
    }
    _fire(employee) { // simulate private method
        // this.dressCode = 'tshirt';
        console.log(employee.firstname + " has been fired! Dress with :" + this.dressCode);
    }
    _hire(employee) { // simulate private method
        console.log(employee.firstname + " has been hired back! Dress with :" + this.dressCode);
    }
    _seminar() { // simulate private method
        this.dressCode = 'suit';
        console.log("He is going to seminar Dress with :" + this.dressCode);
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection. Dress with :" + this.dressCode);
    }
    gossip(employee) {
        console.log("Hey " + employee.firstname, "Today is very cold!");
    }
    talk(message) {
        console.log(message);
    }
    reportRobot(self,reportRobot){
        self.talk(reportRobot);
    }
    async readFileWork() {
        let self = this;
        try {
            let employees = await new Promise(function (resolve, reject) { //hint by Panotz
                fs.readFile('employee9.json', 'utf8', function (err, data) {
                    let employees = [];
                    if(err)
                        reject(err)
                    else { 
                        self.employeesRaw = JSON.parse(data)
                        for(let i = 0; i < self.employeesRaw.length; i++){
                            if(self.employeesRaw[i].role == 'CEO') {
                                employees[i] = new CEO(
                                self.employeesRaw[i].id,
                                self.employeesRaw[i].firstname,
                                self.employeesRaw[i].lastname,
                                self.employeesRaw[i].salary,
                                self.employeesRaw[i].role,
                                self.employeesRaw[i].dressCode)
                            }
                            else if(self.employeesRaw[i].role == 'Programmer') {
                                employees[i] = new Programmer(
                                self.employeesRaw[i].id,
                                self.employeesRaw[i].firstname,
                                self.employeesRaw[i].lastname,
                                self.employeesRaw[i].salary,
                                self.employeesRaw[i].role,
                                self.employeesRaw[i].type)
                            }
                            else if(self.employeesRaw[i].role == 'OfficeCleaner') {
                                employees[i] = new OfficeCleaner(
                                self.employeesRaw[i].id,
                                self.employeesRaw[i].firstname,
                                self.employeesRaw[i].lastname,
                                self.employeesRaw[i].salary,
                                self.employeesRaw[i].role,
                                self.employeesRaw[i].dressCode)
                            }
                        }
                        self.employees = employees;
                        // console.log(data)
                        resolve(self.employees)
                    }
                });
            })
            return (employees);
        } catch (err) {
            console.error(err);
        }
    }
}
exports.CEO = CEO;