const mysql = require('mysql')
class Database {
    constructor() {
        this.connection = mysql.createConnection({
            host        : 'localhost',
            user        : 'me',
            password    : 'secret',
            database    : 'my_db'
        });
    }
    getUsers(callbackFunction) {
        let sql = "SELECT * FROM user";
        this.connection.query(sql, function (err, result) {
            if (err)
                console.error(err);
            else    
                callbackFunction(result);
        })
    }
}
const db = new Database();
db.getUsers(callbackFunction);