const {Employee} = require('./employee.js');

class OfficeCleaner extends Employee {
    constructor(id,firstname, lastname, salary, role, dressCode) {
        super(id,firstname, lastname, salary);
        this.role = role;
        this.dressCode = dressCode;
    }
    work() {  // simulate public method
        this._clean();
        this._killcoachoach();
        this._decorateroom();
        this._welcomeguest();
    }
    _clean() {
        console.log(this.firstname + " is an officecleaner function job to clean.");
    }
    _killcoachoach() {
        console.log(this.firstname + " is an officecleaner function job to killcoachoach.");
    }
    _decorateroom() {
        console.log(this.firstname + " is an officecleaner function job to decorateroom.");
    }
    _welcomeguest() {
        console.log(this.firstname + " is an officecleaner function job to welcomeguest.");
    }
}
exports.OfficeCleaner = OfficeCleaner;