let fs = require('fs');
const {Employee} = require('./employee.js');
const {CEO} = require('./ceo.js');
const {Programmer} = require('./programmer.js');
const {OfficeCleaner} = require('./officecleaner.js');

let somchai = new CEO(1001,"Somchai","Sudlor",30000,'CEO','tshirt');
let victim = new Programmer(1,"Peter", "Parker", 50000, 'Programmer','Front End');

somchai.readFileWork()
.then(data => {
    for (const key in data) {
        data[key].work(victim);
    }
})