const _ = require('lodash');

class MyUtility {
    _assign(obj, source) {
        return _.assign(object,source); 
    }
    _times(num, iteratee) {
        return _.times(n, iteratee);
    }
    _keyBy(arr, param) {
        return _.keyBy(array, param);
    }
    _cloneDeep(value) {
        return _.cloneDeep(value);
    }
    _filter(arr, param) {
        return _.filter(arr, param);
    }
    _sortBy(arr, param) {
        return _.sortBy(arr, param)
    }

}

let fn = new MyUtility();
let users = [
    { 'user': 'fred',   'age': 48 },
    { 'user': 'barney', 'age': 36 },
    { 'user': 'fred',   'age': 40 },
    { 'user': 'barney', 'age': 34 }
  ];
   
  _.sortBy(users, [function(o) { return o.user; }]);
  // => objects for [['barney', 36], ['barney', 34], ['fred', 48], ['fred', 40]]
   
  _.sortBy(users, ['user', 'age']);
  // => objects for [['barney', 34], ['barney', 36], ['fred', 40], ['fred', 48]]

console.log(fn._sortBy(users,['user', 'age']))




