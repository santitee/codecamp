const {db} = require('../lib/db.js');

async function getUser () {
    //query database
    const rows = await db.execute("SELECT * FROM user ");
    return rows;
}

exports.getUser = getUser;