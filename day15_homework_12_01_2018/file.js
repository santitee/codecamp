module.exports = function (db) {
 return {
    async find (id) {
    const [rows] = await this.db.execute(`
        select
            first_name, last_name
        from users
        where id = ?
    `, [id])
    return new User(db, rows[0])
    },
    async findAll () {
    const [rows] = await this.db.execute(`
        select
        first_name, last_name
        from users
    `)
    return rows.map((row) => new User(db, row))
    }
  }
}
