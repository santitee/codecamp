const database = require('../config/database.json');
const mysql = require('mysql2/promise');

class Database {
    constructor() {
        this.host = database["production"].host;
        this.user = database["production"].username;
        this.password = database["production"].password;
        this.database = database["production"].database;
        this.connectionDatabase();
    }
    //
    async connectionDatabase() {
        this.connection = await mysql.createConnection( {
            host : this.host,
            user : this.user,
            database : this.database,
            password : this.password
        });
    }
    async execute(sql) {
        const [rows, fields] = await this.connection.execute(sql);
        return rows;
    }  
}
module.exports.db = new Database() 