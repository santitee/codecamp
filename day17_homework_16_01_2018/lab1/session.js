const Koa = require('koa')
const session = require('koa-session')
const pool = require('mysql2/promise').createPool({host:'localhost',user:'root',database:'codecamp'})

const sessionStore = {}

const sessionConfig = {
  key: 'sess',
  maxAge: 3600 * 1000,
  httpOnly: true,
  store: {
    get (key, maxAge, { rolling }) {
      return sessionStore[key]
    },
    set (key, sess, maxAge, { rolling }) {
      sessionStore[key] = sess
    },
    destroy (key) {
      delete sessionStore[key]
    }
  }
}

const app = new Koa()

app.keys = ['supersecret']

app
  .use(session(sessionConfig, app))
  .use(handler)
  .listen(3000)

function handler (ctx) {
  let n = ctx.session.views || 0
  ctx.session.views = ++n
  const _session = JSON.stringify(ctx.session)

  //
  pool.getConnection()
  try {
    pool.execute(`
    INSERT INTO sessions (id, user) 
      VALUE
        ('${_session}');
    `)
  } finally {
    pool.release()
  }
  //

  ctx.body = `${n} views`
}