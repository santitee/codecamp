// function hello(parameter1, parameter2, parameter3) {
//     console.log('hello'+ parameter1 + parameter2 + parameter3);
// };

// hello('1','2','3');

// function func(callback) {
//     callback(1, 2, 3);
// };

// func()


// func(function(parameter1, parameter2, parameter3){
//     console.log(parameter1 * 2 + parameter2 * 3 + parameter3 * 4)
// });
let hello = function() {
    parameter1 = '1';
    parameter2 = '2';
    callbackFunction = function(){
      console.log('Inside Callback function');
    }; // Declaration
    console.log('hello' + parameter1 + parameter2);
    callbackFunction(); // Execution
  }; // Declaration  

hello('1','2',function(){ // Declaration
    console.log('Inside Callback function');
  }); // Execution  


function hello3() {
    return 1;
}
console.log(hello3); // Function
console.log(hello3()); // 1
  