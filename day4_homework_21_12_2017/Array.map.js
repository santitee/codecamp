let array1 = [1, 4, 9, 16];
const map1 = array1.map(function (x) { return x * 2;
});
console.log(map1);
// expected output: Array [2, 8, 18, 32]

let array1 = [1, 4, 9, 16];
const map1 = array1.map(x => x * 2);
console.log(map1);
// expected output: Array [2, 8, 18, 32]
