let fs = require('fs');
//fs.writeFile('employee.txt', '[{"id":"1001","firstname":"Luke","lastname":"Skywalker","company":"Walt Disney","salary":"40000"},{"id":"1002","firstname":"Tony","lastname":"Stark","company":"Marvel","salary":"1000000"},{"id":"1003","firstname":"Somchai","lastname":"Jaidee","company":"Love2work","salary":"20000"},{"id":"1004","firstname":"Monkey D","lastname":"Luffee","company":"One Piece","salary":"9000000"}]', 'utf8', function(err, data) { 

fs.readFile('homework1-4.json', 'utf8', function (err, data) {
    //console.log(data);
    dataObj = JSON.parse(data);
    // console.log(dataStr);

    const result = dataObj.filter( data => {
        return data.friends.length >= 2 && data.gender == 'male'
      })
      .map (data => { 
        let obj = {};
        obj.name = data.name
        obj.gender = data.gender
        obj.company = data.company
        obj.email = data.email
        obj.friends = data.friends
        obj.balance = data.balance.substring(1).replace(',','')/10
        return obj
      });
    console.log(result);
});

