const mysql = require('mysql2');

class Database {
    constructor ( host, username, password, database ) {
        this.instance = mysql.createConnection({
            host: host,
            user: username,
            password: password,
            database: database
        })
    }
    
    getUser(callback) {
        let sql = "SELECT * FROM user";
        this.instance.query( sql, function (err, result) {
            if(err) console.log(err)
            else {
                callback(result)
            }
        })
    }
}

module.exports = function() {
    return new Database('localhost', 'root', '', 'codecamp')
}