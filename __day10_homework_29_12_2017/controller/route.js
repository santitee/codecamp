
const logger = require('koa-logger');
const models = require('../models/homework10-1.js')

// module.exports = function(conn) {
//     app.use(async (ctx, next) => {
//         try{
//             // get the client
//             const mysql = require('mysql2/promise');
//             // create the connection
//             const conn = await mysql.createConnection(
//                 {
//                     host: 'localhost',
//                     user: 'root',
//                     database: 'codecamp'
//                 }
//             );   
//         }
//     });
// }

module.exports = function (app) {
    app.use(async (ctx, next) => {
        try {
            // get the client
            const mysql = await models.getUser();
            // create the connection
            const connection = await mysql.createConnection(
                {
                    host: 'localhost',
                    user: 'root',
                    database: 'codecamp'
                }
            );
            // query database
            const [rows, fields] = await connection.execute('SELECT * FROM user');
            await ctx.render('user', {
                "data": rows
            })
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });
    app.use(logger());
    app.listen(3000);
}
    // app.use(async (ctx, next) => {
    //     try {
    //     await ctx.render('user', result, fields)
    //     await next();
    //     } catch (err) {
    //     ctx.status = 400
    //     ctx.body = `Uh-oh: ${err.message}`
    //     }
    // });
