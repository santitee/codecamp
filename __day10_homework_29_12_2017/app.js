const Koa = require('koa');

const app = new Koa();
const mainMiddleware = require('./controller/routes.js')(app);
app.listen(3000);