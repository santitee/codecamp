const mysql = require('mysql2');
const config = require('./config/database');
const {host, database, user, password} = config;

class Database {
    constructor(host, database, user, password, connectionLimit) {
        this._host = host;
        this._database = database;
        this._user = user;
        this._password = password;
        this._connectionLimit = connectionLimit;
        this._pool = mysql.createPool({
                connectionLimit : this._connectionLimit,
                host: this._host,
                database: this._database,
                user: this._user,
                password: this._password
            });
    }
    static getInstance() {
        if (!this._instance)
            this._instance = new Database(host, database, user, password, 10);
        return this._instance;
    }
    async execute(query) {
        let buffer = await new Promise((reslove, reject) => {
            this._pool.getConnection(function(err, connection) {
                connection.query(query, (err, results) => {
                    if (err) reject(err);
                    // done with connection
                    connection.release();
                    // resolve
                    reslove(results);
                });
            });
        });
        return buffer;
    }
}

module.exports = Database.getInstance();